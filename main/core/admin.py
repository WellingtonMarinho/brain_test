from django.contrib import admin

from main.core.models import RuralProducer


# Register your models here.
@admin.register(RuralProducer)
class RuralProducer(admin.ModelAdmin):
    pass
