from django.core.exceptions import ValidationError as DjangoValidationError
from django.db.models import Count, Sum, Q
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from main.core.models import RuralProducer
from main.core.serializer import (
    RuralProducerModelSerializer,
    DashboardResponseSerializer,
)


class RuralProducerListAPIView(ListCreateAPIView):
    serializer_class = RuralProducerModelSerializer
    queryset = RuralProducer.objects.all()

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except DjangoValidationError as e:
            return Response({"non_field_errors": e}, status=status.HTTP_400_BAD_REQUEST)


class RuralProducerDetailAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = RuralProducerModelSerializer
    queryset = RuralProducer.objects.all()
    lookup_url_kwarg = "rural_producer_pk"

    def patch(self, request, *args, **kwargs):
        try:
            return super().patch(request, *args, **kwargs)
        except DjangoValidationError as e:
            return Response({"non_field_errors": e}, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        try:
            return super().put(request, *args, **kwargs)
        except DjangoValidationError as e:
            return Response({"non_field_errors": e}, status=status.HTTP_400_BAD_REQUEST)


class DashboardDataAPIView(APIView):
    @extend_schema(responses={200: DashboardResponseSerializer})
    def get(self, request):
        qs = RuralProducer.objects.all()

        total_farms = qs.count()
        by_state = qs.values("state").annotate(total_farms=Count("id"))
        total_area_farms = qs.aggregate(total_area_farms=Sum("total_area"))[
            "total_area_farms"
        ]
        by_planted_crops = qs.aggregate(
            soy_count=Count("pk", filter=Q(planted_crops__icontains="soy")),
            corn_count=Count("pk", filter=Q(planted_crops__contains="corn")),
            cotton_count=Count("pk", filter=Q(planted_crops__contains="cotton")),
            coffee_count=Count("pk", filter=Q(planted_crops__contains="coffee")),
            sugar_cane_count=Count(
                "pk", filter=Q(planted_crops__contains="sugar cane")
            ),
        )
        data = {
            "total_farms": total_farms,
            "total_area": total_area_farms,
            "by_state": by_state,
            "by_planted_crops": by_planted_crops,
        }
        serializer = DashboardResponseSerializer(data=data)
        return Response(serializer.initial_data, status=status.HTTP_200_OK)
