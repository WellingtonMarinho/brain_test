from django.core.management.base import BaseCommand

from tests.conftest import create_rural_producers


class Command(BaseCommand):
    help = "Create fake data to test API's"

    def add_arguments(self, parser):
        parser.add_argument("quantity", nargs="?", type=int, default=500)

    def handle(self, *args, **options):
        quantity = options["quantity"]
        create_rural_producers(quantity=quantity)

        self.stdout.write(
            self.style.SUCCESS(
                f'Successfully generate {quantity} fake Rural Producers"'
            )
        )
