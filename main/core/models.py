from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _

from localflavor.br.br_states import STATE_CHOICES


class RuralProducer(models.Model):
    class PlantedCrops(models.TextChoices):
        SOY = "SOY", _("SOY")
        CORN = "CORN", _("CORN")
        COTTON = "COTTON", _("COTTON")
        COFFEE = "COFFEE", _("COFFEE")
        SUGAR_CANE = "SUGAR_CANE", _("SUGAR CANE")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Created at"))
    modified_at = models.DateTimeField(auto_now=True, verbose_name=_("Modified at"))

    name = models.CharField(_("Name"), max_length=255)
    farm_name = models.CharField(_("Farm Name"), max_length=255)
    cnpj = models.CharField(_("CPNJ"), max_length=18, blank=True, null=True)
    cpf = models.CharField(_("CPF"), max_length=14, blank=True, null=True)
    state = models.CharField(_("State"), max_length=2, choices=STATE_CHOICES)
    city = models.CharField(_("City"), max_length=255)
    total_area = models.DecimalField(
        _("Total Area"), decimal_places=1, max_digits=8, default=00.0
    )
    arable_area = models.DecimalField(
        _("Arable Area"), decimal_places=1, max_digits=8, default=00.0
    )
    vegetation_area = models.DecimalField(
        _("Vegetation Area"), decimal_places=1, max_digits=8, default=00.0
    )
    planted_crops = models.JSONField()

    def __str__(self):
        return f"{self.farm_name}"

    def save(self, *args, **kwargs):
        self.clean()
        return super().save(*args, **kwargs)

    def clean(self):
        self.validate_cnpf_and_cpf_rules(cnpj=self.cnpj, cpf=self.cpf)
        self.validate_area_values(
            total_area=self.total_area,
            arable_area=self.arable_area,
            vegetation_area=self.vegetation_area,
        )
        self.validate_planted_crops(self.planted_crops)

    def validate_cnpf_and_cpf_rules(self, cnpj, cpf):
        if cpf and cnpj:
            raise ValidationError(
                _("Provide CNPJ or CPF, not the both to create Rural Producer")
            )

        if not cpf and not cnpj:
            raise ValidationError(_("Provide CNPJ or CPF, to create Rural Producer"))

    def validate_area_values(self, total_area, arable_area, vegetation_area):
        if sum([arable_area, vegetation_area]) > total_area:
            raise ValidationError(
                _(
                    "The sum of the fields, ARABLE AREA and VEGETATION AREA can't be greater than the TOTAL AREA"
                )
            )

    def validate_planted_crops(self, planted_crops_list):
        choices = self.PlantedCrops.choices
        values_acceptable = [label.lower() for value, label in choices]
        values_not_acceptable = []

        for planted_crop in planted_crops_list:
            if planted_crop not in values_acceptable:
                values_not_acceptable.append(planted_crop)

        if values_not_acceptable:
            raise ValidationError(f"These fields {values_not_acceptable} are not valid")
