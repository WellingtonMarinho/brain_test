from django.utils.translation import gettext as _
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from main.core.models import RuralProducer
from validate_docbr import CPF, CNPJ


@extend_schema_field(
    {
        "type": "string",
        "format": "json",
        "example": ["soy", "corn", "cotton", "coffee", "sugar cane"],
    },
)
class CustomJsonFieldSerializer(serializers.JSONField):
    pass


class RuralProducerModelSerializer(serializers.ModelSerializer):
    planted_crops = CustomJsonFieldSerializer()
    cnpj = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    cpf = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    class Meta:
        model = RuralProducer
        exclude = ["created_at", "modified_at"]

    def validate_cnpj(self, value):
        if not value:
            return

        validator = CNPJ()
        is_valid = validator.validate(value)

        if is_valid:
            return value

        raise serializers.ValidationError(_(f"This CNPJ {value} is not valid"))

    def validate_cpf(self, value):
        if not value:
            return

        validator = CPF()
        is_valid = validator.validate(value)

        if is_valid:
            return value

        raise serializers.ValidationError(_(f"This CPF {value} is not valid"))


class ByPlantedCropsResponseSerializer(serializers.Serializer):
    coffee_count = serializers.IntegerField()
    corn_count = serializers.IntegerField()
    cotton_count = serializers.IntegerField()
    soy_count = serializers.IntegerField()
    sugar_cane_count = serializers.IntegerField()


class ByStateResponseSerializer(serializers.Serializer):
    state = serializers.CharField()
    total_farms = serializers.IntegerField()


class DashboardResponseSerializer(serializers.Serializer):
    by_planted_crops = ByPlantedCropsResponseSerializer()
    by_state = ByStateResponseSerializer()
    total_area = serializers.IntegerField()
    total_farms = serializers.IntegerField()
