from django.urls import path

from main.core.views import (
    RuralProducerListAPIView,
    RuralProducerDetailAPIView,
    DashboardDataAPIView,
)


urlpatterns = [
    path(
        "rural_producers/",
        RuralProducerListAPIView.as_view(),
        name="rural-producer-create-api-view",
    ),
    path(
        "rural_producers/<int:rural_producer_pk>/",
        RuralProducerDetailAPIView.as_view(),
        name="rural-producer-detail-api-view",
    ),
    path(
        "rural_producers/dashboard/",
        DashboardDataAPIView.as_view(),
        name="rural-producer-dashboard-api-view",
    ),
]
