import random

import pytest
from model_bakery.baker import make
from faker import Faker

from main.core.models import RuralProducer


fake = Faker("pt_BR")
fake.random
fake.random.getstate()
PLANTED_CROPS = ["soy", "corn", "cotton", "coffee", "sugar cane"]


@pytest.fixture
def initial_payload_with_cnpf_valid():
    payload = {
        "cnpj": "65.797.394/0001-04",
        "name": "João da Silva",
        "farm_name": "Fazenda Sorriso do Jaguar",
        "city": "Campinas",
        "state": "SP",
        "total_area": 47.8,
        "arable_area": 35.2,
        "vegetation_area": 10.5,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }
    return payload


@pytest.fixture
def initial_payload_with_cpf_valid():
    payload = {
        "cpf": "991.681.868-19",
        "name": "João da Silva",
        "farm_name": "Fazenda Sorriso do Jaguar",
        "city": "Campinas",
        "state": "SP",
        "total_area": 47.80,
        "arable_area": 35.20,
        "vegetation_area": 10.50,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }
    return payload


@pytest.fixture
def rural_producer(db, initial_payload_with_cpf_valid):
    rural_producer = RuralProducer.objects.create(**initial_payload_with_cpf_valid)
    return rural_producer


def get_distinct_planted_crops():
    planted_crops = random.choices(PLANTED_CROPS, k=2)

    while planted_crops[0] == planted_crops[1]:
        choice = random.choices(PLANTED_CROPS)[0]
        planted_crops[-1] = choice
    return planted_crops


def create_rural_producers(quantity):
    for index in range(quantity):
        cpf = ""
        cnpj = fake.cnpj()

        if index % 2 == 0:
            cpf = fake.cpf()
            cnpj = ""

        make(
            RuralProducer,
            name=fake.name(),
            farm_name=fake.company(),
            cnpj=cnpj,
            cpf=cpf,
            state=fake.estado()[0],
            city=fake.city(),
            total_area=80,
            vegetation_area=30,
            arable_area=40,
            planted_crops=get_distinct_planted_crops(),
        )


@pytest.fixture
def create_rural_producers_factory(db):
    def wrapper(quantity=50):
        create_rural_producers(quantity=quantity)

    return wrapper
