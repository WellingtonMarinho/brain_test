import pytest
from django.core.exceptions import ValidationError

from main.core.models import RuralProducer


def test_validate_cnpf_and_cpf_rules_with_two_data():
    with pytest.raises(ValidationError):
        rural_producer = RuralProducer()
        rural_producer.validate_cnpf_and_cpf_rules(
            cpf="18862765010", cnpj="48400121000157"
        )


def test_validate_cnpf_and_cpf_rules_without_any_data():
    with pytest.raises(ValidationError):
        rural_producer = RuralProducer()
        rural_producer.validate_cnpf_and_cpf_rules(cpf="", cnpj="")


def test_validate_cnpf_and_cpf_rules_with_cnpj():
    rural_producer = RuralProducer()
    result = rural_producer.validate_cnpf_and_cpf_rules(cpf=None, cnpj="48400121000157")

    assert result is None


def test_validate_cnpf_and_cpf_rules_with_cpf():
    rural_producer = RuralProducer()
    result = rural_producer.validate_cnpf_and_cpf_rules(cpf="18862765010", cnpj=None)

    assert result is None


def test_validate_arable_area_with_vegetation_area_cant_than_more_total_area_success():
    rural_producer = RuralProducer()
    result = rural_producer.validate_area_values(
        total_area=50.0,
        arable_area=25.0,
        vegetation_area=25,
    )
    assert result is None


def test_validate_arable_area_with_vegetation_area_cant_than_more_total_area_fails():
    rural_producer = RuralProducer()

    with pytest.raises(
        ValidationError,
        match="The sum of the fields, ARABLE AREA and VEGETATION AREA can't be greater than the TOTAL AREA",
    ):
        rural_producer.validate_area_values(
            total_area=50.0,
            arable_area=26.0,
            vegetation_area=25,
        )
