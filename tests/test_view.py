import pytest
from django.db.models import Count, Q
from rest_framework import status
from rest_framework.reverse import reverse

from main.core.models import RuralProducer


def test_create_rural_producer_using_cnpj_api_view_success(
    db, client, initial_payload_with_cnpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cnpf_valid
    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == {
        "id": 1,
        "name": "João da Silva",
        "farm_name": "Fazenda Sorriso do Jaguar",
        "state": "SP",
        "city": "Campinas",
        "cnpj": "65.797.394/0001-04",
        "cpf": None,
        "total_area": 47.8,
        "arable_area": 35.2,
        "vegetation_area": 10.5,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }


def test_create_rural_producer_using_cpf_api_view_success(
    db, client, initial_payload_with_cpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cpf_valid
    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_201_CREATED
    last_rural_producer_created_pk = RuralProducer.objects.last().pk
    assert response.json() == {
        "id": last_rural_producer_created_pk,
        "cnpj": None,
        "cpf": "991.681.868-19",
        "name": "João da Silva",
        "farm_name": "Fazenda Sorriso do Jaguar",
        "city": "Campinas",
        "state": "SP",
        "total_area": 47.8,
        "arable_area": 35.2,
        "vegetation_area": 10.5,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }


def test_create_rural_producer_using_cpf_api_view_fails_because_has_no_valid_planted_crops(
    db, client, initial_payload_with_cpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cpf_valid
    payload["planted_crops"] = ["soy", "coffee", "sugar cane", "Whatever"]

    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "non_field_errors": ["These fields ['Whatever'] are not valid"]
    }


def test_create_rural_producer_using_cpf_api_view_fails_because_has_no_valid_planted_crops_another_case(
    db, client, initial_payload_with_cpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cpf_valid
    payload["planted_crops"] = ["cevada", "sugar cane", "Whatever"]

    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        "non_field_errors": ["These fields ['cevada', 'Whatever'] are not valid"]
    }


def test_create_rural_producer_using_cpf_api_view_fails_because_vegetation_area_and_arable_area_greater_than_total_area(
    db, client, initial_payload_with_cpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cpf_valid
    payload["vegetation_area"] = 15.5
    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    expected_error = {
        "non_field_errors": [
            "The sum of the fields, ARABLE AREA and VEGETATION AREA can't be greater than the TOTAL AREA"
        ]
    }
    assert response.json() == expected_error


@pytest.mark.parametrize(
    "invalid_cnpj", ["22.414.237/51", "86.408.552/0001-52", "63.001.143/0001-88"]
)
def test_create_rural_producer_using_invalid_cnpj_api_view_success(
    db, client, initial_payload_with_cnpf_valid, invalid_cnpj
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cnpf_valid
    payload["cnpj"] = invalid_cnpj

    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"cnpj": [f"This CNPJ {invalid_cnpj} is not valid"]}


@pytest.mark.parametrize(
    "invalid_cpf", ["443.858.154.65", "957.279.780-88", "243.440.440-40"]
)
def test_create_rural_producer_using_invalid_cpf_api_view_success(
    db, client, initial_payload_with_cnpf_valid, invalid_cpf
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cnpf_valid
    payload["cpf"] = invalid_cpf

    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"cpf": [f"This CPF {invalid_cpf} is not valid"]}


def test_create_rural_producer_sent_cnpf_and_cpf_must_fails_because_just_one_is_acceptable(
    db, client, initial_payload_with_cnpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cnpf_valid
    payload["cpf"] = "13169028030"

    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    expected_error = {
        "non_field_errors": [
            "Provide CNPJ or CPF, not the both to create Rural Producer"
        ]
    }

    assert response.json() == expected_error


def test_create_rural_producer_sent_without_cnpf_or_cpf_must_fails_because_one_is_required(
    db, client, initial_payload_with_cnpf_valid
):
    path = reverse("rural-producer-create-api-view")

    payload = initial_payload_with_cnpf_valid
    del payload["cnpj"]

    response = client.post(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    expected_error = {
        "non_field_errors": ["Provide CNPJ or CPF, to create Rural Producer"]
    }

    assert response.json() == expected_error


def test_update_total_area_of_rural_producer_api_view_success(
    db, client, rural_producer
):
    path = reverse("rural-producer-detail-api-view", args=[rural_producer.pk])
    expected_total_area = 52.8
    assert rural_producer.total_area != expected_total_area

    payload = {"total_area": expected_total_area}

    response = client.patch(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_200_OK
    expected_response = {
        "id": rural_producer.pk,
        "cnpj": None,
        "cpf": "991.681.868-19",
        "name": "João da Silva",
        "farm_name": "Fazenda Sorriso do Jaguar",
        "city": "Campinas",
        "state": "SP",
        "total_area": expected_total_area,
        "arable_area": 35.2,
        "vegetation_area": 10.5,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }
    assert response.json() == expected_response

    rural_producer.refresh_from_db()
    assert float(rural_producer.total_area) == expected_total_area


def test_update_vegetation_area_of_rural_producer_api_view_fails_because_is_not_valid(
    db, client, rural_producer
):
    path = reverse("rural-producer-detail-api-view", args=[rural_producer.pk])
    expected_vegetation_area = 42.8
    assert rural_producer.vegetation_area != expected_vegetation_area

    payload = {"vegetation_area": expected_vegetation_area}

    response = client.patch(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    expected_error = {
        "non_field_errors": [
            "The sum of the fields, ARABLE AREA and VEGETATION AREA can't be greater than the TOTAL AREA"
        ]
    }

    assert response.json() == expected_error

    rural_producer.refresh_from_db()
    assert float(rural_producer.total_area) != expected_vegetation_area


def test_put_update_total_area_of_rural_producer_api_view_fails_because_is_not_valid(
    db, client, rural_producer, initial_payload_with_cpf_valid
):
    path = reverse("rural-producer-detail-api-view", args=[rural_producer.pk])
    expected_total_area = 10
    assert rural_producer.total_area != expected_total_area

    payload = initial_payload_with_cpf_valid
    payload["total_area"] = expected_total_area

    response = client.put(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    expected_error = {
        "non_field_errors": [
            "The sum of the fields, ARABLE AREA and VEGETATION AREA can't be greater than the TOTAL AREA"
        ]
    }

    assert response.json() == expected_error

    rural_producer.refresh_from_db()
    assert float(rural_producer.total_area) != expected_total_area


def test_update_farm_name_of_rural_producer_api_view_success(
    db, client, rural_producer
):
    path = reverse("rural-producer-detail-api-view", args=[rural_producer.pk])
    expected_farm_name = "Grande Lago"
    assert rural_producer.farm_name != expected_farm_name

    payload = {"farm_name": expected_farm_name}

    response = client.patch(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_200_OK
    expected_response = {
        "id": rural_producer.pk,
        "cnpj": None,
        "cpf": "991.681.868-19",
        "name": "João da Silva",
        "farm_name": expected_farm_name,
        "city": "Campinas",
        "state": "SP",
        "total_area": 47.80,
        "arable_area": 35.2,
        "vegetation_area": 10.5,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }
    assert response.json() == expected_response

    rural_producer.refresh_from_db()
    assert rural_producer.farm_name == expected_farm_name


def test_update_name_of_rural_producer_api_view_success(db, client, rural_producer):
    path = reverse("rural-producer-detail-api-view", args=[rural_producer.pk])
    expected_name = "Ednaldo Marinho"
    assert rural_producer.name != expected_name

    payload = {"name": expected_name}

    response = client.patch(path, data=payload, content_type="application/json")

    assert response.status_code == status.HTTP_200_OK
    expected_response = {
        "id": rural_producer.pk,
        "cnpj": None,
        "cpf": "991.681.868-19",
        "name": expected_name,
        "farm_name": "Fazenda Sorriso do Jaguar",
        "city": "Campinas",
        "state": "SP",
        "total_area": 47.80,
        "arable_area": 35.2,
        "vegetation_area": 10.5,
        "planted_crops": ["soy", "coffee", "sugar cane"],
    }
    assert response.json() == expected_response

    rural_producer.refresh_from_db()
    assert rural_producer.name == expected_name


def test_delete_rural_producer_api_view_success(db, client, rural_producer):
    count_expected = RuralProducer.objects.count() - 1
    path = reverse("rural-producer-detail-api-view", args=[rural_producer.pk])

    response = client.delete(path)

    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert count_expected == RuralProducer.objects.count()


def test_response_information_to_dashboard_success(
    db, client, create_rural_producers_factory
):
    quantity_expected = 500
    create_rural_producers_factory(quantity=quantity_expected)

    qs = RuralProducer.objects.all()
    by_state = qs.values("state").annotate(total_farms=Count("id"))
    by_planted_crops = qs.aggregate(
        soy_count=Count("pk", filter=Q(planted_crops__icontains="soy")),
        corn_count=Count("pk", filter=Q(planted_crops__contains="corn")),
        cotton_count=Count("pk", filter=Q(planted_crops__contains="cotton")),
        coffee_count=Count("pk", filter=Q(planted_crops__contains="coffee")),
        sugar_cane_count=Count("pk", filter=Q(planted_crops__contains="sugar cane")),
    )

    payload_expected = {
        "total_farms": quantity_expected,
        "total_area": 80.0 * quantity_expected,
        "by_state": list(by_state),
        "by_planted_crops": by_planted_crops,
    }
    path = reverse("rural-producer-dashboard-api-view")

    response = client.get(path)

    assert response.status_code == status.HTTP_200_OK
    assert response.json() == payload_expected
