##  Pré-requisitos

    Docker
    Docker Compose

## Tecnologias Utilizadas
    Python 3.11
    Django
    Django Rest Framework
    Postgres
    Pytest


# Como Rodar o projeto:
    Copie o conteúdo de env.sample para um arquivo .env
    $ cp env.sample .env
    
    Rode os seguintes comandos para Build e Up do Docker Compose 
    $ docker compose build
    $ docker compose up
    
    Rodar testes automatizados
    $ docker exec -ti app pytest


## Informações pertinentes
    O projeto rodará em localhost:8000
    Ao rodar o comando de subir o Docker um command Django criará 500 dados distintos automáticamente
    
    Se for necessário criar mais dados basta rodar o comando com o número necessário (default 500):
    $ docker exec -ti app python manage.py generate_rural_producers 2500 

    O projeto conta com 26 testes que validam casos de uso de forma automática.
    Nos testes temos validações para casos de sucesso e de falha, auxiliando na manutenção e evolução saudável do código.

    Há um Swagger disponível como documentação de apoio da API em localhost:8000/api/v1/schema/swagger-ui/

### Considerações sobre decisões de implementação
    Optei por separar as Culturas plantadas em uma coluna Json no Postgres pela simplicidade. 
    Dependendo do contexto e volumetria faria sentido analisar se criar uma tabela e ligar por chave estrangeira seria uma solução melhor
    Mas é o tipo de otimização que exige estudo de caso, e acredito que não se faz necessário, de qualquer forma é uma alternativa viável
    
    Optei também por não criar camada de serviço pela escopo ser reduzido
    Na medida em que tenhamos contextos mais complexos, o ideal seria centralizar as regras em uma classe de serviço
    Evitando regras de negócio na camada de Views ou de Models, isso mantém o código altamente testável e fácil de dar manutenção.
