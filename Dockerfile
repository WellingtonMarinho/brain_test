FROM python:3.11-slim

ARG DEPLOY_PATH='/app'
ARG VERSION=''

ENV VERSION=$VERSION

RUN mkdir -p $DEPLOY_PATH

ADD main/ $DEPLOY_PATH/main
ADD manage.py $DEPLOY_PATH/manage.py
ADD requirements.txt $DEPLOY_PATH/requirements.txt

WORKDIR $DEPLOY_PATH

RUN pip install -U pip setuptools
RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["gunicorn", "main.wsgi:application"]
